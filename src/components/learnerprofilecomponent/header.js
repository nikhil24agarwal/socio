import React from "react";
import "./header.css";

export default function Header() {
  return (
    <div className="full-base">
      <div className="content">
        <i class="fas fa-envelope-open-text fa-3x"></i>
        <span className="span1">Message</span>
        <i class="fas fa-envelope-open-text fa-3x"></i>
        <span className="span2">Home</span>
        <i class="fas fa-envelope-open-text fa-3x"></i>
        <span className="span3">Notification</span>
        <i class="fas fa-envelope-open-text fa-3x"></i>
        <span className="span4">Profile</span>
        <i class="fas fa-envelope-open-text fa-3x"></i>
        <span className="span5">Mentor</span>
      </div>
    </div>
  );
}
