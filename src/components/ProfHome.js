import { React, useState } from "react";
import Pnavbar from "./professionalprofilecomponents/pnavbar";
import Searchbox from "./professionalprofilecomponents/searchbox";
import Topprofile from "./professionalprofilecomponents/Topprofile";
import Post from "./professionalprofilecomponents/Post";
import Explore from "./professionalprofilecomponents/Explore";
import Trending from "./professionalprofilecomponents/Trending";
import Communities from "./professionalprofilecomponents/Communities";
import Message from "./professionalprofilecomponents/Message";
import "./professionalprofilecomponents/Trending.css";

export default function ProfHome() {
  const [postDataArray, setPostDataArray] = useState([]);
  const [postTitleArray, setPostTitleArray] = useState([]);

  let postComps = postDataArray.map((curEl, i) => {
    return <Post postdata={curEl} posttitle={postTitleArray[i]} />;
  });

  return (
    <div>
      <Pnavbar />
      <div className="forcolor">
        <Searchbox
          postDataArray={postDataArray}
          setPostDataArray={setPostDataArray}
          postTitleArray={postTitleArray}
          setPostTitleArray={setPostTitleArray}
        />
        <Topprofile />
        <Explore />
        <Trending />
        <Communities />
        <Message />
        {postComps}
        <Post
          posttitle="If you have Excel 365, it’s time to leave VLOOKUP behind!"
          postdata="You’ve heard the job title #dataanalyst floating around lately and it sounds like an intriguing career. The idea of working with data and technology has piqued your curiosity, but what does a data #analyst do all day?
                    Generally speaking, a data analyst will retrieve and gather #data, organize it and use it to reach meaningful conclusions.Data analysts may be responsible for designing dashboards, monitoring KPI’s, and maintaining relationship databases.
                    They also evaluate systems for different departments throughout their organization using #businessintelligence software, visualization software, and computer programming.
                    Most jobs in data #analytics involve gathering and cleaning complex data sets to uncover trends/correlations/patterns and business insights and identify new opportunities for process improvement.
                    Most jobs in data #analytics involve gathering and cleaning complex data sets to uncover trends/correlations/patterns and business insights"
        />
        <Post
          posttitle="Hey I am title"
          postdata="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        />
      </div>
    </div>
  );
}
