import "bootstrap/dist/css/bootstrap.min.css";
import "./Joinuscss.css";
import React from "react";
import Plogin from "./joinuscomponents/plogin";
import Llogin from "./joinuscomponents/llogin";
import PloginModal from "./joinuscomponents/PloginModal";

function Joinus() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <div>
      <PloginModal show={modalShow} onHide={() => setModalShow(false)} />
      <div className="joinus-heading"> Join Us as! </div>
      <div className="joinus-component">
        <Plogin setPLoginModal={setModalShow} />
        <span className="joinus-span" />
        <Llogin />
      </div>
    </div>
  );
}

export default Joinus;
