import React from "react";
import "./pnavbar.css";

export default function Pnavbar() {
  return (
    <>
      <div className="Pnav">
        <input type="text" placeholder="Search.." className="nav-texti" />
        <i class="fas fa-search icon fa-1.5x"></i>

        <span className="message-icon1">
          <i class="fas fa-envelope-open-text fa-2x"></i>
          <span>Message</span>
        </span>
        <span className="message-icon2">
          <i class="fas fa-home fa-2x"></i>
          <span>Home</span>
        </span>
        <span className="message-icon3">
          <i class="fas fa-bell fa-2x"></i>
          <span>Notification</span>
        </span>
        <span className="message-icon4">
          <i class="fas fa-user-circle fa-2x"></i>
          <span>Profile</span>
        </span>
      </div>
    </>
  );
}
