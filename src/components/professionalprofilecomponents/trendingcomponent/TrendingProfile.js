import React from "react";
import "./trendingprofile.css";

export default function TrendingProfile(props) {
  return (
    <div className="trending-div">
      <i class="fas fa-user-circle fa-3x trending-icon"></i>
      <div className="trending-allinone">
        <div className="trending-div1">{props.name}</div>
        <div className="trending-div2">{props.trendingin}</div>
        <div className="trending-div3">{props.topic}</div>
      </div>
    </div>
  );
}
