import React from "react";
import "./Communities.css";

export default function Communities() {
  return (
    <div className="community-box">
      <span className="community">Communities</span>
      <input type="text" placeholder="Search.." className="community-text" />
      <i class="fas fa-search community-icon fa-1.5x"></i>

      <div className="community-topic">Data Science</div>
      <div className="community-topic">Artificil Intelligence</div>
      <div className="community-topic">Blockchain</div>
      <button className="community-button">See more</button>
    </div>
  );
}
