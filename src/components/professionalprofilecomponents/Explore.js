import React from "react";
import "./Explore.css";
import Field from "./expolreboxcomponent/Field";

export default function Explore() {
  return (
    <div className="boxx">
      <span className="explore"> Explore</span>
      <input type="text" placeholder="Search.." className="nav-textii" />
      <i class="fas fa-search iconn fa-1.5x"></i>
      <Field field="Artificial Intelligence" />
      <Field field="Data Science" />
      <Field field="BlockChain" />
      {/* <Field field="Machine Learning" /> */}
      <button className="explore-button">See more</button>
    </div>
  );
}
