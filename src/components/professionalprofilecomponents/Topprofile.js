import React from "react";
import "./Topprofile.css";
import Profile from "./searchboxelement/profile";

export default function Topprofile() {
  return (
    <div className="boxup3">
      <div class="writing">Top Profile as per your interest.</div>
      <Profile name="Evan Williams" designation="CEO of Medium" />
      <Profile name="Sunder Pichai" designation="CEO of Google" />
      <Profile name="Satya Nadella" designation="CEO of Microsoft" />

      <button className="searchbox-button">See more</button>
    </div>
  );
}
