import React from "react";
import "./Trending.css";
import TrendingProfile from "./trendingcomponent/TrendingProfile";

export default function Trending() {
  return (
    <div className="trending-box">
      <div className="trending-text">Trending posts as per your interest</div>
      <TrendingProfile
        trendingin="Trending in Artificial Intellgence"
        topic="Deploy CNN model using Pyplot"
        name="Mark Rose · AI Engineer at Google"
      />
      <TrendingProfile
        trendingin="Trending in Web Development"
        topic="All about React Hooks"
        name="Harsh priya · Web Developer at Tesla"
      />
      <button className="trending-button">See more</button>
    </div>
  );
}
