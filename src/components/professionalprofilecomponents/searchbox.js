import { React, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./searchbox.css";
import { Modal, Button, ButtonToolbar } from "react-bootstrap";
import Heading from "./postcompenents/Heading";
import { event } from "jquery";

export default function Searchbox({
  postDataArray,
  setPostDataArray,
  setPostTitleArray,
  postTitleArray,
}) {
  const [show, setShow] = useState(false);
  const [data, setData] = useState(null);
  const [title, setTitle] = useState(null);
  const [tag, setTag] = useState(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function textwriting(val) {
    setData(val);
  }
  function titlewriting(val) {
    setTitle(val);
  }

  function postit() {
    setPostDataArray((postDataArray) => [...postDataArray, data]);
    setPostTitleArray((postTitleArray) => [...postTitleArray, title]);
  }

  function postBtnClick() {
    postit();
    handleClose();
  }

  const [domainName, setDomainName] = useState("Domain-Name");

  return (
    <div className="boxup1">
      <i class="fas fa-user-circle fa-3x picon"></i>
      <input
        class="searchbox-input"
        placeholder="Educate the world"
        onClick={handleShow}
      />

      <Modal
        size="l"
        show={show}
        onHide={handleClose}
        animation={false}
        dialogClassName="searchbox-modal"
        className="modal-full"
      >
        <Modal.Body>
          <div className="searchBox-heading-container">
            <Heading domainName={domainName} />
            <div className="dot-movement dropdown">
              <button
                className="button-dot dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <div class="dots">•••</div>
              </button>

              <div
                class="dropdown-menu dropdown-styling"
                aria-labelledby="dropdownMenuButton"
              >
                <a class="dropdown-item" href="#">
                  option
                </a>
                <a class="dropdown-item" href="#">
                  option
                </a>
                <a class="dropdown-item" href="#">
                  option
                </a>
              </div>
            </div>
          </div>
          <textarea
            onChange={(event) => titlewriting(event.target.value)}
            rows="2"
            cols="50"
            placeholder="Title"
            className="post-title"
          ></textarea>

          <textarea
            onChange={(event) => textwriting(event.target.value)}
            rows="16"
            cols="55"
            placeholder="Educate the World"
            className="post-text"
          ></textarea>

          <div className="searchBox-bottom-container">
            <input
              className="searchBox-tags-input"
              placeholder="Tags/Skills/Post Essence"
            ></input>
            <button onClick={postBtnClick} className="post-button">
              Post
            </button>
            <input
              className="searchBox-tags-input"
              placeholder="Choose the best “DOMAIN”"
              onChange={(event) =>
                setDomainName(
                  event.target.value.length > 0
                    ? event.target.value
                    : "Domain-Name"
                )
              }
            ></input>
          </div>
          <hr className="post-buttonline" />
          <div className="post-taglines">
            Excel • Technology • Data Analysis • Data Analysis
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
