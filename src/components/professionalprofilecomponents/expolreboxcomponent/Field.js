import React from "react";
import "./Field.css";

export default function Field(props) {
  return (
    <div className="field-box">
      <span className="field-span">{props.field}</span>
      <button className="field-follow">Follow</button>
    </div>
  );
}
