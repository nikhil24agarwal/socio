import React from "react";
import "./Heading.css";

export default function Heading({ domainName }) {
  return (
    <div className="heading-div">
      <i class="fas fa-user-circle fa-3x heading-icon"></i>
      <div className="heading-allinone">
        <div className="heading-div1">
          Team ShareX · Building an efficient culture around learning and wisdom
        </div>
        <div className="heading-div2">
          Published in {domainName?.length > 0 ? domainName : "Domain-Name"} ·
          7h{" "}
        </div>
      </div>
    </div>
  );
}
