import React from "react";
import "./profile.css";

export default function Profile(props) {
  return (
    <div>
      <div className="profile-box">
        <i class="fas fa-user-circle fa-3x profile-icon"></i>
        <span className="span11"> {props.name}</span>
        {/* <br /> */}
        <span className="span22">{props.designation}</span>
      </div>
    </div>
  );
}
