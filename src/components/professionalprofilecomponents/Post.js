import { React, useState } from "react";
import "./Post.css";
import Heading from "./postcompenents/Heading";
import Commentbox from "./commentbox/commentbox";

export default function Post({ postdata, posttitle }) {
  const [set, setfunction] = useState(false);

  if (set) {
    document.querySelector("boxup2");
  }
  return (
    <div
      className="boxup2"
      onMouseEnter={() => setfunction(true)}
      onMouseLeave={() => setfunction(false)}
    >
      {/* <Commentbox /> */}
      <Heading />
      <div className="post-title"> {posttitle}</div>

      <div className="post-content">{postdata}</div>
      <hr className="line"></hr>
      <div className="forflexingbutton">
        <button className="button-react">React</button>
        <button className="button-express">Sample</button>
        <button className="button-express">Express</button>
      </div>
      <hr className="bottom-line" />
      <div className="tagname">
        Excel · Technology · Data · Analysis · Data Analysis
      </div>

      {/* <hr className="line2" /> */}
    </div>
  );
}
