import React from "react";
import { Switch, Route } from "react-router-dom";
import { Joinus, LearnerProfile, ProfHome } from "./index";

export default function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Joinus} />
        <Route exact path="/joinus" component={Joinus} />
        <Route exact path="/hello" component={LearnerProfile} />
        <Route exact path="/home" component={ProfHome} />
      </Switch>
    </div>
  );
}
