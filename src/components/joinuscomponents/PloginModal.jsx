import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./PloginModal.css";
import { Modal } from "react-bootstrap";

function PloginModal(props) {
  const [modalPage, setModalPage] = React.useState(1);

  let modalBody;

  if (modalPage == 1) {
    modalBody = (
      <div>
        <h1 className="plogin-heading">Create Account</h1>
        <div className="input-container">
          <input
            className="input"
            type="text"
            placeholder="Email or Phone"
          ></input>
          <input className="input" type="text" placeholder="Full Name"></input>
          <input className="input" type="text" placeholder="Password"></input>
          <input
            className="input"
            type="text"
            placeholder="Date of Birth"
          ></input>
          <p className="text-smallest">
            By signing up, you agree to our Terms , Data Policy and Cookies
            Policy .
          </p>
          <button className="btn-long" onClick={() => setModalPage(2)}>
            Sign Up
          </button>
        </div>
      </div>
    );
  } else if (modalPage == 2) {
    modalBody = (
      <div className="input-container">
        <input className="input" type="text" placeholder="Code"></input>
        <p className="text-modalpage2">Enter a code that we have sent to you</p>
        <button onClick={() => setModalPage(3)}>Next</button>
      </div>
    );
  } else if (modalPage == 3) {
    modalBody = (
      <div className="input-container">
        <p>
          Please share something that reflects your professional journey. Since
          we want to include those professionals who really want to share their
          learnings so that others can learn from your experience.<br></br>
          <br></br> You can share any possible URLs that reflect the best of
          your professional side.<br></br> For example<br></br> 1. LinkedIn URL
          <br></br>
          2. Medium URL <br></br>3. Personal Website<br></br>
          <br></br> Once You’ll submit, our team will contact you.<br></br>
          <br></br> You can share more than one URL (share atleast one URL)
        </p>
        <div className="container-row">
          <input
            className="input input-modalPage3"
            type="text"
            placeholder="URL"
          ></input>
          <p className="mediumText-modalPage3">Add more URL</p>
        </div>
        <button onClick={() => setModalPage(4)}>Submit</button>
      </div>
    );
  } else if (modalPage == 4) {
    modalBody = (
      <div className="input-container">
        <p>Thanks for submission, our team will contact you soon!</p>
      </div>
    );
  }

  return (
    <Modal
      {...props}
      size="m"
      dialogClassName="modal-90w"
      aria-labelledby="example-custom-modal-styling-title"
      centered
    >
      <Modal.Body>{modalBody}</Modal.Body>
    </Modal>
  );
}

export default PloginModal;
