import React from "react";
import "./llogin.css";

export default function Llogin() {
  return (
    <div className="login-border">
      <h1 className="heading">Novice</h1>
      <a className="below-headingg" href="www.google.com">
        Novice: a person new to and inexperienced in a job or situation. <br />{" "}
        Ex. Students, New working professional
      </a>
      <br />
      <br />
      <input className="input extra" placeholder="Email or Phone"></input>
      <br />
      <input className="input" placeholder="Password"></input>
      <br />
      <button>Log In</button>

      <a href="www.google.com" className="below-button">
        Forget Password
      </a>

      <div className="last-message">
        Don't have an account?
        <a href="www.google.com">
          <b> Sign Up</b>
        </a>
      </div>
    </div>
  );
}
