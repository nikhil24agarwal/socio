import React from "react";
import "./plogin.css";

export default function Plogin({ setPLoginModal }) {
  return (
    <div className="login-border">
      <h1 className="heading"> Experienced Professional</h1>
      <a className="below-heading" href="www.google.com">
        know more about it and its <br /> purpose
      </a>
      <br />
      <br />
      <input className="input extra" placeholder="Email or Phone"></input>
      <br />
      <input className="input" placeholder="Password"></input>
      <br />
      <button>
        <a className="profloginbutton" href="home">
          Log In
        </a>
      </button>

      <a href="#" className="below-button">
        Forget Password
      </a>

      <div className="last-message">
        Don't have an account?
        <a className="btn-signUp" onClick={() => setPLoginModal(true)}>
          {" "}
          Sign Up
        </a>
      </div>
    </div>
  );
}
